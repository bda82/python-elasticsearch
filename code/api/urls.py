from django.urls import path
from django.contrib.auth.views import LogoutView

from api.views.elastic import ElasticViewSet
from api.views.note import (
    NoteListView,
    NoteDetailView,
    NoteCreateView,
    NoteUpdateView,
    NoteDeleteView,
)
from api.views.auth import (
    CustomLoginView,
    CustomRegisterView,
)

app_name = 'api'

# auth
urlpatterns = [
    path('login/', CustomLoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(next_page='api:login'), name='logout'),
    path('register/', CustomRegisterView.as_view(), name='register'),
]

# note
urlpatterns += [
    path('note/', NoteListView.as_view(), name='notes'),
    path('note/<int:pk>/', NoteDetailView.as_view(), name='note'),
    path('note/create/', NoteCreateView.as_view(), name='note-create'),
    path('note/edit/<int:pk>/', NoteUpdateView.as_view(), name='note-edit'),
    path('note/delete/<int:pk>/', NoteDeleteView.as_view(), name='note-delete'),
]

# elastic
urlpatterns += [
    path('elastic/', ElasticViewSet.as_view({"get": "list"}), name='elastic'),
]
