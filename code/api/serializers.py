from django_elasticsearch_dsl_drf.serializers import DocumentSerializer
from api.documents import NoteDocument
from api.models import Note


class NoteDocumentSerializer(DocumentSerializer):
    class Meta:
        model = Note
        document = NoteDocument
        fields = ("title", "description", "body")

        @staticmethod
        def get_location(obj):
            try:
                return obj.location
            except Exception as e:
                print(f"{e = }")
                return {}
