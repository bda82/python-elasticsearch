from django_elasticsearch_dsl_drf.viewsets import DocumentViewSet
from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    CompoundSearchFilterBackend,
)

from api.documents import NoteDocument
from api.serializers import NoteDocumentSerializer


class ElasticViewSet(DocumentViewSet):
    """
    to rebuild index, call:
        python code/manage.py search_index --rebuild
    to search, call
        http://localhost:8000/elastic/
    to search with query, call (trouble is the one of Note titles)
        http://localhost:8000/elastic/?search=trouble
    """
    document = NoteDocument
    serializer_class = NoteDocumentSerializer
    filter_backends = [
        FilteringFilterBackend,
        CompoundSearchFilterBackend,
    ]
    search_fields = ("title", "description", "body")
    multi_match_search_fields = ("title", "description", "body")
    filter_fields = {
        "title": "title",
        "description": "description",
        "body": "body"
    }
