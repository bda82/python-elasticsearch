from django.urls import reverse_lazy
from api.models import Note
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin


class NoteListView(LoginRequiredMixin, ListView):
    model = Note
    context_object_name = "notes"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["notes"] = context["notes"].filter(user=self.request.user)

        search_input = self.request.GET.get("search-area") or ""

        if search_input:
            context["notes"] = context["notes"].filter(
                title__icontains=search_input
            )

        context["search_input"] = search_input

        return context


class NoteDetailView(LoginRequiredMixin, DetailView):
    model = Note
    context_object_name = "note"


class NoteCreateView(LoginRequiredMixin, CreateView):
    model = Note
    fields = [
        "title",
        "description",
        "body",
        "link",
        "due_to",
    ]
    success_url = reverse_lazy("api:notes")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(NoteCreateView, self).form_valid(form)


class NoteUpdateView(LoginRequiredMixin, UpdateView):
    model = Note
    fields = [
        "title",
        "description",
        "body",
        "link",
        "due_to",
    ]
    success_url = reverse_lazy("api:notes")


class NoteDeleteView(LoginRequiredMixin, DeleteView):
    model = Note
    context_object_name = "note"
    success_url = reverse_lazy("api:notes")