from django.contrib.auth.views import LoginView
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect
from django.views.generic.edit import FormView
from django.contrib.auth import login
from django.urls import reverse_lazy


class CustomLoginView(LoginView):
    fields = "__all__"
    template_name = "auth/login.html"
    redirect_authenticated_user = True

    def get_success_url(self):
        return reverse_lazy("api:notes")


class CustomRegisterView(FormView):
    template_name = "auth/register.html"
    form_class = UserCreationForm
    redirect_authenticated_user = True
    success_url = reverse_lazy("api:notes")

    def form_valid(self, form):
        user = form.save()
        if user:
            login(self.request, user)
        return super(CustomRegisterView, self).form_valid(form)

    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect("api:notes")
        return super(CustomRegisterView, self).get(*args, **kwargs)
