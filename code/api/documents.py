from django.contrib.auth import get_user_model
from django_elasticsearch_dsl import Document, fields, Index
from api.models import Note

User = get_user_model()


PUBLISHER_INDEX = Index("elastic_demo")
PUBLISHER_INDEX.settings(number_of_shards=1, number_of_replicas=1)


@PUBLISHER_INDEX.doc_type
class NoteDocument(Document):
    id = fields.IntegerField(attr="id")
    title = fields.TextField(
        fields={
            "raw": {
                "type": "keyword"
            }
        }
    )
    description = fields.TextField(
        fields={
            "raw": {
                "type": "keyword"
            }
        }
    )
    body = fields.TextField(
        fields={
            "raw": {
                "type": "keyword"
            }
        }
    )

    class Django:
        model = Note
