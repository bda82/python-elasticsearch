from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse


class Note(models.Model):
    title = models.CharField(max_length=128)
    description = models.CharField(default='', max_length=255)
    body = models.TextField(default='')
    link = models.TextField(default='', null=True, blank=True)
    done = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(null=True, blank=True)
    due_to = models.DateTimeField(null=True, blank=True)

    user = models.ForeignKey(
        User,
        related_name='notes',
        on_delete=models.CASCADE
    )

    objects = models.Manager()

    def __str__(self):
        return f"<Note [self.id]: {self.title}>"

    def get_absolute_utl(self):
        return reverse("note_detail", args=[str(self.id)])
