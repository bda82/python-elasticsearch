import random

from itertools import repeat

from dateutil.tz import tz
from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.core.management.base import BaseCommand
from faker import Faker

from api.models import Note

faker = Faker()
User = get_user_model()


class Command(BaseCommand):
    help = 'Create random Notes'

    def add_arguments(self, parser):
        parser.add_argument('--count', action='append', type=int, help='Indicates the number of Notes to be created')

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        if not count:
            count = 10

        if isinstance(count, list) and count:
            count = count[0]

        admin = User.objects.filter(email="admin@example.com").first()
        if not admin:
            print('--> Create Admin')
            User.objects.create_superuser("admin", "admin@example.com", "admin")

        [f(admin) for f in repeat(self.create_note, count)]

    @staticmethod
    def create_note(user):
        title = faker.word()
        if not Note.objects.filter(title=title):
            note = Note(
                title=title,
                description=faker.word(),
                body=faker.word(),
                user=user,
            )
            note.save()
            print(f'---> Create Note: {title}')
            return note
